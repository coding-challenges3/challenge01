# README

All `CREATE TABLE`, `INSERT` and both `SELECT` queries are saved in _db.sql_ file. Queries have been tested at https://www.db-fiddle.com/f/bhZ2ZunFmeu5vgdSD8xe5h/3

The solutions have been highlighted also in this README.

### Solution for Task A1
```sql
WITH
days AS (
    SELECT generate_series(min(publish::date), max("delete"::date), '1 day') AS day
    FROM ads
),
mix AS (
    SELECT day, ads.id AS "ad_id"
    FROM days
    LEFT JOIN ads ON days.day between ads."publish" AND ads."delete"
)
SELECT
to_char(mix.day, 'YYYY-MM-DD') AS "Date",
count(ad_id) AS "Ads visible on this day"
FROM mix
GROUP BY mix.day
ORDER BY 1;
```


### Solution for Task A2
```sql
WITH years AS (
    SELECT generate_series(min(date_trunc('year',publish)), max(date_trunc('year',"delete")), '1 year') AS year
    FROM ads
),
mix AS (
    SELECT years.year AS "year", ads.id AS "ad_id", date_trunc('year',ads.publish) AS "y", date_trunc('year',ads."delete") AS "d"
    FROM years
    LEFT JOIN ads ON (years.year between date_trunc('year',ads.publish) AND date_trunc('year',ads."delete"))
    -- the following adds 'Tipperary' and 'Beef Cattle' filters
    LEFT JOIN region AS reg ON reg.id = ads.region_id
    LEFT JOIN category AS cat ON cat.id = ads.category_id
    WHERE reg.county = 'Tipperary'
        AND cat.subsubcategory ilike '%beef%cattl%'
),
ads_per_year AS (
    SELECT
    to_char(year, 'YYYY') AS year,
    count(ad_id) AS visible
    FROM mix
    GROUP BY year
    ORDER BY 1
),
yoy_growth AS (
    SELECT year::INT AS "Year", visible AS "Ads visible throughout this year",
    CASE
        WHEN (LAG(visible) OVER (ORDER BY year)) IS NOT NULL
            THEN concat((visible - (LAG(visible) OVER (ORDER BY year)))::float / ((LAG(visible) OVER (ORDER BY year)))::float * 100, ' %')
        ELSE NULL END AS "YoY growth"
    FROM ads_per_year
)
SELECT "Year", "YoY growth" FROM yoy_growth WHERE "Year" = 2017
```

### Solution for Task B

Task B has been solved using _pandas_ and _numpy_. The solution is in the IPython notebook [Task_B.ipynb](Task_B.ipynb).

Car data with standardized mileage are saved in file [standardised_data_sample.csv](standardised_data_sample.csv).

I took extra step and tried to find some missing values. The estimates are saved in file [standardised_data_sample_with_less_missing_values.csv](standardised_data_sample_with_less_missing_values.csv).
