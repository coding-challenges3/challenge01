-- https://www.db-fiddle.com/f/bhZ2ZunFmeu5vgdSD8xe5h/0

CREATE TABLE region(
    id INT PRIMARY KEY NOT NULL,
    country VARCHAR(100),
    province VARCHAR(100),
    county VARCHAR(100),
    town VARCHAR(100)
);

INSERT INTO region(id, country, province, county, town) VALUES
(1, 'Republic of Ireland', 'Munster', 'Tipperary', 'Ahenny'),
(2, 'Republic of Ireland', 'Munster', 'Tipperary', 'Cashel'),
(3, 'Republic of Ireland', 'Leinster', 'Kildare', 'Naas'),
(4, 'Republic of Ireland', 'Leinster', 'Kildare', 'Newbridge');


CREATE TABLE category (
    id INT PRIMARY KEY NOT NULL,
    vertical VARCHAR(100),
    "main category" VARCHAR(100),
    category VARCHAR(100),
    subsubcategory VARCHAR(100)
);

INSERT INTO category (id, vertical, "main category", category, subsubcategory) VALUES
(1, 'Farming', 'Farming', 'Livestock', 'Beef Cattle'),
(2, 'Farming', 'Farming', 'Livestock', 'Sheep Cattle'),
(3, 'Farming', 'Farming', 'Livestock', 'Horses');


CREATE TABLE ads(
    id SERIAL PRIMARY KEY NOT NULL,
    category_id INT REFERENCES category(id) ON DELETE CASCADE,
    region_id INT REFERENCES region(id) ON DELETE CASCADE,
    "create" timestamp default now(),
    publish timestamp,
    "delete" timestamp
);

INSERT INTO ads(category_id, region_id, "create", publish, "delete") VALUES
(1, 1, '2018-06-01', '2018-06-01', '2018-06-02'),
(1, 2, '2018-06-01', '2018-06-01', '2018-06-03'),
(2, 3, '2018-06-02', '2018-06-02', '2018-06-08'),
(2, 4, '2018-06-02', '2018-06-10', '2018-06-12'),
(3, 1, '2018-06-03', '2018-06-10', '2018-06-12'),
(3, 2, '2018-06-04', '2018-06-11', '2018-06-22'),
(1, 1, '2018-06-04', '2018-06-11', '2018-06-22'),
(1, 2, '2018-06-04', '2018-06-12', '2018-06-22'),
(1, 1, '2018-06-05', '2018-06-15', '2018-06-22'),
(1, 2, '2018-06-05', '2018-06-16', '2018-06-22'),
(1, 1, '2018-06-05', '2018-06-17', '2018-06-22');

INSERT INTO ads(category_id, region_id, "create", publish, "delete") VALUES
(1, 1, '2015-02-01', '2015-02-01', '2018-06-02'), -- adds 1 to 2015,2016,2017,2018
(1, 1, '2015-02-01', '2015-02-01', '2018-06-02'), -- adds 1 to 2015,2016,2017,2018
(1, 1, '2016-02-01', '2016-02-01', '2016-06-02'), -- adds 1 to 2016
(1, 1, '2016-02-01', '2016-02-01', '2016-06-02'); -- adds 1 to 2016


-- Task A1

WITH
days AS (
    SELECT generate_series(min(publish::date), max("delete"::date), '1 day') AS day
    FROM ads
),
mix AS (
    SELECT day, ads.id AS "ad_id"
    FROM days
    LEFT JOIN ads ON days.day between ads."publish" AND ads."delete"
)
SELECT
to_char(mix.day, 'YYYY-MM-DD') AS "Date",
count(ad_id) AS "Ads visible on this day"
FROM mix
GROUP BY mix.day
ORDER BY 1;



-- Task A2

WITH years AS (
    SELECT generate_series(min(date_trunc('year',publish)), max(date_trunc('year',"delete")), '1 year') AS year
    FROM ads
),
mix AS (
    SELECT years.year AS "year", ads.id AS "ad_id", date_trunc('year',ads.publish) AS "y", date_trunc('year',ads."delete") AS "d"
    FROM years
    LEFT JOIN ads ON (years.year between date_trunc('year',ads.publish) AND date_trunc('year',ads."delete"))
    -- the following adds 'Tipperary' and 'Beef Cattle' filters
    LEFT JOIN region AS reg ON reg.id = ads.region_id
    LEFT JOIN category AS cat ON cat.id = ads.category_id
    WHERE reg.county = 'Tipperary'
        AND cat.subsubcategory ilike '%beef%cattl%'
),
ads_per_year AS (
    SELECT
    to_char(year, 'YYYY') AS year,
    count(ad_id) AS visible
    FROM mix
    GROUP BY year
    ORDER BY 1
),
yoy_growth AS (
    SELECT year::INT AS "Year", visible AS "Ads visible throughout this year",
    CASE
        WHEN (LAG(visible) OVER (ORDER BY year)) IS NOT NULL
            THEN concat((visible - (LAG(visible) OVER (ORDER BY year)))::float / ((LAG(visible) OVER (ORDER BY year)))::float * 100, ' %')
        ELSE NULL END AS "YoY growth"
    FROM ads_per_year
)
SELECT "Year", "YoY growth" FROM yoy_growth WHERE "Year" = 2017
